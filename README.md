# mingle8

mingle8 is a tool to encode basic text in a way that the character limit of services like Twitter can be extended. Currently two normal characters fit into one special UTF-8 character. Only characters below 0x10FFFF can be used which contain the full ASCII charset and most characters of the western languages.

I would like to tweet the following sentence:

"You can search throughout the entire universe for someone who is more deserving of your love and affection than you are yourself, and that person is not to be found anywhere. You yourself, as much as anybody in the entire universe deserve your love and affection."

It's a bit too long. Twitter only accepts 140 characters, but the sentence is 263 characters long. Mingle8 to the rescue!

To try it out, download the Java file from the [releases](https://github.com/steckerhalter/mingle8/releases) and run:

    java -jar mingle8-0.0.1-standalone.jar -e "You can search throughout the entire universe for someone who is more deserving of your love and affection than you are yourself, and that person is not to be found anywhere. You yourself, as much as anybody in the entire universe deserve your love and affection."

This will print:

    𬣞𺡀𱣂𷁀𹣊𰣤𱣐𐃨𴃤𷣪𳣐𷣪𺁀𺃐𲡀𲣜𺃒𹃊𐃪𷃒𻃊𹃦𲡀𳃞𹁀𹣞𶣊𷣜𲡀𻣐𷡀𴣦𐃚𷣤𲡀𲃊𹣊𹃬𴣜𳡀𷣌𐃲𷣪𹁀𶃞𻃊𐃂𷃈𐃂𳃌𲣆𺃒𷣜𐃨𴃂𷁀𼣞𺡀𰣤𲡀𼣞𺣤𹣊𶃌𖁀𰣜𲁀𺃐𰣨𐃠𲣤𹣞𷁀𴣦𐃜𷣨𐃨𷡀𱃊𐃌𷣪𷃈𐃂𷃲𻣐𲣤𲡜𐂲𷣪𐃲𷣪𹃦𲣘𳁘𐃂𹡀𶣪𱣐𐃂𹡀𰣜𼣄𷣈𼡀𴣜𐃨𴃊𐃊𷃨𴣤𲡀𺣜𴣬𲣤𹣊𐃈𲣦𲣤𻃊𐃲𷣪𹁀𶃞𻃊𐃂𷃈𐃂𳃌𲣆𺃒𷣜𗀀

We have just brought number of characters down from 263 to 132. You could now tweet the encoded text and the receiver can convert it back to text with:

    java -jar mingle8-0.0.1-standalone.jar -d 𬣞𺡀𱣂𷁀𹣊𰣤𱣐𐃨𴃤𷣪𳣐𷣪𺁀𺃐𲡀𲣜𺃒𹃊𐃪𷃒𻃊𹃦𲡀𳃞𹁀𹣞𶣊𷣜𲡀𻣐𷡀𴣦𐃚𷣤𲡀𲃊𹣊𹃬𴣜𳡀𷣌𐃲𷣪𹁀𶃞𻃊𐃂𷃈𐃂𳃌𲣆𺃒𷣜𐃨𴃂𷁀𼣞𺡀𰣤𲡀𼣞𺣤𹣊𶃌𖁀𰣜𲁀𺃐𰣨𐃠𲣤𹣞𷁀𴣦𐃜𷣨𐃨𷡀𱃊𐃌𷣪𷃈𐃂𷃲𻣐𲣤𲡜𐂲𷣪𐃲𷣪𹃦𲣘𳁘𐃂𹡀𶣪𱣐𐃂𹡀𰣜𼣄𷣈𼡀𴣜𐃨𴃊𐃊𷃨𴣤𲡀𺣜𴣬𲣤𹣊𐃈𲣦𲣤𻃊𐃲𷣪𹁀𶃞𻃊𐃂𷃈𐃂𳃌𲣆𺃒𷣜𗀀

Which should give you the original sentence back.

Hooray! We have broken the tweet limit!
