;; UTF-8 4 bytes sequence:
;;
;; 1st Byte    2nd Byte    3rd Byte    4th Byte    Maximum Expressible Unicode Value
;; 11110xxx    10xxxxxx    10xxxxxx    10xxxxxx    10FFFF hex
;;      |       Char 1       ||     Char 2   |

(ns mingle8
  (:require [clojure.tools.cli :as c])
  (:gen-class))

(def basemask 2r11110000)
(def extmask 2r10000000)

(defn uc1 [c1p1]
  (bit-shift-left (bit-or basemask c1p1) 24))

(defn uc2 [c1p2]
  (bit-shift-left (bit-or extmask c1p2) 16))

(defn uc3 [c1p3 c2p1]
  (bit-shift-left (bit-or extmask c1p3 c2p1) 8))

(defn uc4 [c2p2]
  (bit-or extmask c2p2))

(defn valid-char? [char]
  (when (> (int char) 0x3ff)
    (throw (Exception. (format "Error: Your text contains the character `%s' with a value %s. The maximum allowed value is %s" char (int char) (int 0x3ff))))))

(defn valid-text? [text]
  (doseq [char text]
    (valid-char? char)))

(defn mingle8 [text]
  (for [[a b] (partition 2 2 [0] text)]
    (bit-or
     (uc1 (bit-shift-right (int a) 7))
     (uc2 (bit-and (bit-shift-right (int a) 1) 2r111111))
     (uc3
      (bit-shift-left (bit-and (int a) 2r1) 5)
      (bit-and (bit-shift-right (int b) 5) 2r11111))
     (uc4
      (bit-and (bit-shift-left (int b) 1) 2r111111)))))

(defn unmingle8 [chars]
  [(char
    (bit-or
     (bit-shift-left (bit-and 2r111 (nth chars 0)) 8)
     (bit-shift-left (bit-and 2r111111 (nth chars 1)) 1)
     (bit-and 2r1 (bit-shift-right (nth chars 2) 5))))
   (char
    (bit-or
     (bit-shift-left (bit-and 2r11111 (nth chars 2)) 5)
     (bit-and 2r11111 (bit-shift-right (nth chars 3) 1))))])

(defn decode [text]
  (apply str (mapcat unmingle8 (partition-all 4 (map #(bit-and (int %) 0xFF) (.getBytes text))))))

(defn long-to-bytes [long]
  [(.byteValue (bit-shift-right long 24))
   (.byteValue (bit-shift-right long 16))
   (.byteValue (bit-shift-right long 8))
   (.byteValue (bit-and 2r11111111 long))])

(defn make-byte-array [longs]
  (byte-array
   (reduce concat (map long-to-bytes longs))))

(defn encode [text]
  (valid-text? text)
  (String.
   (make-byte-array
    (mingle8 text))
   "UTF-8"))

(defn -main [& args]
  (let [[options args banner]
        (c/cli args
               ["-e" "--encode" "Text to encode"]
               ["-d" "--decode" "Text to decode"])]
    (cond
     (:encode options) (println (encode (:encode options)))
     (:decode options) (println (decode (:decode options))))))
