(defproject mingle8 "0.0.1"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/tools.cli "0.3.1"]]
  :repl-options {:init-ns mingle8}
  :main mingle8
  :aot [mingle8]
  :manifest {"Built-By" "steckerhalter"})
